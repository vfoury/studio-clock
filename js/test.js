function animate() {
    requestAnimationFrame(animate)

    c.clearRect(0, 0, canvas.width, canvas.height);

    c.beginPath();
    c.arc(x, y, 40, 0, Math.PI * 2, false);
    c.strokeStyle = 'rgba(200,0,0,1)';
    c.stroke();

    c.fillStyle = 'rgba(0,0,0,1)';
    c.fillRect(x, y, 100, 100);
    x++


}
let x = 0;
let y = 0;
let canvas = document.getElementById("myCanvas");
let c = canvas.getContext("2d");
animate()