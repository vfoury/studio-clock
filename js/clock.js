window.onload = function() {
    function drawCircles(image, from, to) {
        for (let i = from; i <= to; i++) {
            let angd = ((360 / 60) * i) - 90;
            let angr = (angd) * (Math.PI / 180);
            //let r = rayon
            let x = Math.cos(angr) * rayon;
            let y = Math.sin(angr) * rayon;
            let tempx = Math.round((xcenter) + x - (ledsize / 2));
            let tempy = Math.round((ycenter) + y - (ledsize / 2));
            ctx.drawImage(image, tempx, tempy, ledsize, ledsize);
        }
    }

    function updateSecondsLeds() {
        let second = new Date().getSeconds();
        if (second == 1) {
            drawCircles(greenled, 1, 1);
            drawCircles(redled, 2, 59);
            drawCircles(redled, 0, 0);
        } else {
            let endgreen = second + (((Math.abs(second - 60)) / 60) | 0) * 60;
            drawCircles(greenled, 1, endgreen);
            drawCircles(redled, endgreen + 1, 59);
        }
    }

    function updateClock() {
        let now = new Date();
        let displayhour = String(now.getHours()).padStart(2, "0");
        let displayminute = String(now.getMinutes()).padStart(2, "0");
        let displaysecond = String(now.getSeconds()).padStart(2, "0");

        // basic text properties
        ctx.textBaseline = "alphabetic";
        ctx.textAlign = "left";

        //// Afficher l'heure
        //cleanup circle
        ctx.fillStyle = "black";
        ctx.arc(xcenter, ycenter, rayon - ledsize / 2, 0, 2 * Math.PI, true);
        ctx.fill();
        // calcul des dimensions des textes avant affichage
        let hmfont = String(fontsize * (rayon / 90)) + 'px digital7';
        ctx.font = hmfont;
        let hmtext = String(displayhour) + ":" + String(displayminute);
        let hmwidth = ctx.measureText(hmtext).width;
        //
        let sfont = String(fontsize * (rayon / 190)) + 'px digital7';
        ctx.font = sfont;
        let stext = ":" + String(displaysecond);
        let swidth = ctx.measureText(stext).width;
        //
        let hmswidth = hmwidth + swidth;
        //x time position
        let xtpos = 2; //2 is the x center of the clock / >2 will shift right
        //y time position
        let ytpos = 15; // >15 will get closer to the center (down)/ <15 will shift up
        //y date position
        let ydpos = 3; // >3 will get closer to the center (up)/ <3 will shift down
        //display hour:minutes
        ctx.font = hmfont;
        ctx.fillStyle = "#111111";
        ctx.fillText("88:88",
            xcenter - hmswidth / xtpos,
            ycenter - (rayon / ytpos));
        ctx.fillStyle = "red";
        ctx.fillText(hmtext,
            xcenter - hmswidth / xtpos,
            ycenter - (rayon / ytpos));
        //display :seconds
        ctx.font = sfont;
        ctx.fillStyle = "#111111";
        ctx.fillText(":88",
            xcenter - (hmswidth / xtpos) + hmwidth,
            ycenter - (rayon / ytpos));
        ctx.fillStyle = "red";
        ctx.fillText(stext,
            xcenter - (hmswidth / xtpos) + hmwidth,
            ycenter - (rayon / ytpos));

        //if ((displayhour + displayminute + displaysecond) == "000000") {
        //updateDate(now);
        ////Afficher la date
        ctx.font = String(fontsize * (rayon / 200)) + 'px digital14';
        ctx.textBaseline = "middle";
        ctx.textAlign = "center";
        displaydate = now.toLocaleString('en-FR', {
            day: 'numeric', // numeric, 2-digit
            year: 'numeric', // numeric, 2-digit
            month: 'long' // numeric, 2-digit, long, short, narrow
        });
        ctx.fillText(displaydate,
            xcenter,
            ycenter + (rayon / ydpos));
        displayday = now.toLocaleString('en-FR', {
            weekday: 'long' // long, short, narrow
        });
        ctx.fillText(displayday,
            xcenter,
            ycenter + (rayon / ydpos) + fontsize * (rayon / 200));
        //}

    }

    let canvas = document.getElementById("myCanvas");
    let wwidth = window.innerWidth;
    let wheight = window.innerHeight;
    //canvas.width = wwidth - 10;
    //canvas.height = wheight - ((wheight) * (wheight / 30000));
    canvas.width = wwidth - 10;
    canvas.height = wheight - 30;
    let ctx = canvas.getContext("2d");
    let redled = document.getElementById("redled");
    let blueled = document.getElementById("blueled");
    let greenled = document.getElementById("greenled");
    let xcenter = canvas.width / 2;
    let ycenter = canvas.height / 2;
    let ledsize = Math.min(canvas.height, canvas.width) / 25;
    let fontsize = 30;
    let rayon = Math.min(canvas.height, canvas.width) / 2 - ledsize / 2;

    /*ctx.drawImage(blueled,
        xcenter - ledsize / 2,
        ycenter - ledsize / 2, ledsize, ledsize);*/ // show center with blue led
    drawCircles(redled, 0, 59);
    setInterval(updateSecondsLeds, 1000);
    setInterval(updateClock, 1000);
    //setInterval(function() { updateClock() }, 1000);
    //setInterval(function() { console.log("tttt") }, 5000);
}
window.onresize = function() { location.reload(); }